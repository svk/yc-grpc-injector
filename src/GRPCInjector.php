<?php

namespace svk\GRPCInjector;

class GRPCInjector {
    public function __construct() {
        if (!extension_loaded('grpc')) {
            dl('grpc.'.phpversion().'.so');
        }
    }
}
